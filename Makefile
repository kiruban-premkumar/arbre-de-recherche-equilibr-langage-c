# Le nom de l'exécutable final
EXEC = arbre

# Les options de compilations
CFLAGS = -Wall -g

# Librairies
LD_OPTIONS = -lm

# Compilateur
CC = gcc

# Principal rule
all : ${EXEC}

# Rules to construct the final program in GUI MODE
${EXEC} : main.o abr.o isi.o
	${CC} ${CFLAGS} -o $@ $^ ${LD_OPTIONS}

# construction des objets
main.o : main.c abr.h isi.h
	${CC} ${CFLAGS} -c $<

abr.o : abr.c abr.h isi.h
	${CC} ${CFLAGS} -c $<

isi.o : isi.c isi.h abr.h
	${CC} ${CFLAGS} -c $<

# purge des objets
clean:
	rm -f *.o
	rm -f arbre.pdf arbre.dot arbre.exe

# purge des objets et de l'exécutable
clean-all: clean
	rm -f ${EXEC}