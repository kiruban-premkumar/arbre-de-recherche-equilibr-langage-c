#ifndef ABR_H
#define ABR_H

typedef struct _Tnoeud Tnoeud;

Tnoeud* abr_new();
void abr_free(Tnoeud *a);
int abr_hauteur(Tnoeud *a);
void abr_calculerHauteur(Tnoeud **a);
void abr_ajouter(Tnoeud **a, char *contenu, int e, int g, int d);
void abr_equilibrer(Tnoeud **a);
void abr_afficher(FILE *f, Tnoeud *a);
void abr_dot(Tnoeud *a, int x);

#endif /*ABR_H*/