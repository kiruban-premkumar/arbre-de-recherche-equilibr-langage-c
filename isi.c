#include <stdio.h>
#include <stdlib.h>
#include <getopt.h>
#include "isi.h"
#include "abr.h"

/**
 * \struct _Config
 * \brief Structure pour les options
 */
struct _Config
{
	int g;
	int d;
	int e;
	int x;
	FILE *fichier;
};

/**
 * \brief Affichage d'un manuel d'utilisation
 */
static void isi_help(char **argv)
{
	printf("[PROGRAM]\n");
    printf("%s\n",argv[0]);
    printf("        Ce programme permet de construire et de visualiser des arbres binaires de chaînes de caractères.\n");
    printf("[OPTIONS]\n");
    printf("-g\n");
    printf("		Dans ce cas tous les noeuds de l'arbre n'ont qu'un seul fils et la plus grande chaîne de caractères se trouve à la racine.\n");
    printf("-d\n");
    printf("        Dans ce cas tous les noeufs de l'arbre n'ont qu'un seul fils et la plus petite chaîne de caractères se trouve à la racine.\n");
   	printf("-e\n");
    printf("        Dans ce cas l'arbre généré est équilibré.\n");
    printf("-x\n");
    printf("        Cette option affiche directement l'arbre en lançant les commandes dot et display.\n");
    exit(0);
}

Config* isi_new()
{
	Config* ret = (Config*)malloc(sizeof(*ret));
	return ret;
}

void isi_init(Config *config)
{
	config->g=0;
	config->d=0;
	config->e=0;
	config->x=0;
	config->fichier = NULL;
}

void isi_readOptions(Config *config, int argc, char **argv)
{
	int opt,i;
    FILE *tmp = NULL;

	for(i = 1; i < argc; i++)
	{
		tmp = fopen(argv[i],"r");
		if(tmp != NULL)
		{
			config->fichier = fopen(argv[i],"r"); 
			if(i == 1)
			{
				optind = 2;
			}
		}
	}

    static struct option long_options[] = {
    	{"file", 1, 0, 0}
    };
    int option_index = 0;

    while((opt = getopt_long(argc,argv,"gdexh",long_options, &option_index)) != -1)
    {
        switch(opt)
        {
        	case 'g':
                config->g = 1;
                break;
            case 'd':
             	config->d = 1;
                break;
            case 'e':
                config->e = 1;
                break;
            case 'x':
                config->x = 1;
                break;
            case 'h':
                isi_help(argv);
            default:
                break;
        }
    }

    if(config->fichier == NULL)
    	config->fichier = stdin;
}

void isi_processing(Config *config)
{
	int r,i;
	i=0;
	char contenu[100];
	Tnoeud *arbre = NULL;
	while(!feof(config->fichier))
	{
		r = fscanf(config->fichier,"%s",contenu);
		if(r==1)
		{
			if(i==0)
			{
				arbre = abr_new(0,contenu,0);
			}
			else
			{
				abr_ajouter(&arbre,contenu,config->e,config->g,config->d);
			}
			i++;
		}
	}
	abr_dot(arbre,config->x);
}

int isi_verification(Config *config)
{
	if(config->g == 1 && config->d == 1)
		return 1;
	if((config->g == 1 && config->e == 1) || (config->d == 1 && config->e == 1))
		return 2;

	return 0;
}