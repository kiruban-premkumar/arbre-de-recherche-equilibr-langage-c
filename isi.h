typedef struct _Config Config;

/**
 * \brief Allocation de la structure contenant les options
 * \return Config* Pointeur sur la structure allouée
 */
Config* isi_new();

/**
 * \brief Initialisation de la structure contenant les options
 * \param config Structure des options
 * \return void
 */
void isi_init(Config *c);

/**
 * \brief Lit les options
 * \param config Structure des options
 * \param argc Nombre d'arguments
 * \param argv Tableau contenant les arguments
 * \return void
 */
void isi_readOptions(Config *config, int argc, char **argv);

/**
 * \brief Génére l'arbre
 * \param config Les options de génération
 * \param gridB La grille B à comparer.
 * \return void
 */
void isi_processing(Config *config);

/**
 * \brief Verification des options
 * \param config Les options de génération à verifier
 * \return int  1 : si utilisation des options -g et -d ensemble
 *				2 : si utilisation d'une option a fils unique (-g ou -d) avec l'option d'équilibrage -e
 *				0 : Pas de souci
 */
int isi_verification(Config *config);