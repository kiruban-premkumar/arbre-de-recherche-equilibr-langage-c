/**
 * \file main.c
 * \brief Constructeur et visualiseur d'arbres
 * \author Kiruban PREMKUMAR & Souleyman BAH
 * \version 4.2
 * \date Avril 5, 2013
 *
 * Constructeur et visualiseur d'arbres binaires de chaînes de caractères.
 *
 */
#include <stdio.h>
#include <stdlib.h>

#include "abr.h"
#include "isi.h"

int main(int argc, char **argv)
{
    int r;

    Config *config = isi_new();
    isi_init(config);

    isi_readOptions(config,argc,argv);

    r = isi_verification(config);
    
    switch(r)
    {
    	case 1:
    		fprintf(stderr, "%s: vous ne pouvez pas utiliser l'option -d et -g ensemble.\n", argv[0]);
    		break;
    	case 2:
    		fprintf(stderr, "%s: vous ne pouvez pas utiliser l'option d'equilibrage -e avec les options -d ou -g qui n'ont qu'un seul fils.\n", argv[0]);
    		break;
   		default:
   			isi_processing(config);
   			break;
    }
    return 0;
}
